import React from "react";
import { ReactComponent as ButtonAdd } from "../../assets/img/components/add-button.svg";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Col,
  Button,
  Form,
  FormGroup,
  Input,
  Label,
} from "reactstrap";

class ModalFormPemesanan extends React.Component {
  state = {
    jenisDokumen: "",
    nomorDokumen: null,
    tanggalDokumen: null,
    fileDokumen: null, 
  };
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  handleUpload = e => {
    const file = e.target.files[0];
    this.setState({
      fileDokumen: file
    })
  }
  onSubmit = e => {
    e.preventDefault();
    this.props.handleSubmit(this.state);
    this.props.modalToggle();
  }
  render() {
    const { modalToggle, modalOpen } = this.props;
    return (
      <React.Fragment>
        <ButtonAdd
          onClick={modalToggle}
          style={{
            justifySelf: "end",
            marginBottom: "20px",
            cursor: "pointer",
          }}
        />
        <Modal isOpen={modalOpen} toggle={modalToggle}>
          <ModalHeader toggle={modalToggle}>
            Tambah Dokumen Persyaratan{" "}
          </ModalHeader>
          <ModalBody style={{ padding: "3rem" }}>
            <Form onSubmit={this.onSubmit}>
              <FormGroup row>
                <Label for="NomorDokumen" sm={4}>
                  Nomor Dokumen
                </Label>
                <Col sm={8}>
                  <Input
                    onChange={this.handleChange}
                    min="0"
                    type="number"
                    name="nomorDokumen"
                    id="NomorDokumen"
                    required
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="exampleSelect" sm={4}>
                  Jenis Dokumen
                </Label>
                <Col sm={8}>
                  <Input
                    onChange={this.handleChange}
                    type="select"
                    name="jenisDokumen"
                    id="exampleSelect"
                    required
                  >
                    <option disabled>Pilih Jenis Dokumen</option>
                    <option>INVOICE</option>
                    <option>PACKING LIST</option>
                    <option>SKEP TPB (Jika Asal Barang KB/GB/PLB)</option>
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="exampleDatetime" sm={4}>
                  Tanggal Dokumen
                </Label>
                <Col sm={8}>
                  <Input
                    onChange={this.handleChange}
                    type="date"
                    name="tanggalDokumen"
                    id="exampleDate"
                    placeholder="date placeholder"
                    required
                  />
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="exampleFile" sm={4}>
                  Upload Dokumen
                </Label>
                <Col sm={8}>
                  <Input
                    onChange={this.handleUpload}
                    type="file"
                    name="fileDokumen"
                    id="exampleFile"
                    required
                  />
                </Col>
              </FormGroup>
              <div className="mt-4" style={{ display: "grid" }}>
                <Button
                  style={{
                    justifySelf: "center",
                    padding: "8px 55px",
                  }}
                  color="primary"
                >
                  Submit
                </Button>
              </div>
            </Form>
          </ModalBody>
        </Modal>
      </React.Fragment>
    );
  }
}

export default ModalFormPemesanan;
