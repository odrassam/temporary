import React, { Component } from "react";
import ModalButton from "./Dashboard-modal";

class Dashboard extends Component {
  state = {
    modalOpen: false,
  };
  modalToggle = () => {
    this.setState((prevState) => ({
      modalOpen: !prevState.modalOpen,
    }));
  };
  handleSubmit = (value) => {
    console.log(value);
  };
  render() {
    const { modalOpen } = this.state;
    return (
      <div className="container" style={{ display: "grid" }}>
        <ModalButton
          modalOpen={modalOpen}
          modalToggle={this.modalToggle}
          handleSubmit={this.handleSubmit}
        />
        <table className="table table-bordered mb-4">
          <thead
            style={{ backgroundColor: "rgba(2, 39, 93, 1)", color: "white" }}
          >
            <tr className="text-center">
              <th scope="col">No</th>
              <th scope="col">Jenis Dokumen</th>
              <th scope="col">Nomor Dokumen</th>
              <th scope="col">Tanggal Dokumen</th>
              <th scope="col">Dokumen</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row"></th>
              <td> </td>
              <td> </td>
              <td> </td>
              <td> </td>
            </tr>
          </tbody>
        </table>
        <button
          className="btn btn-primary"
          style={{
            justifySelf: "end",
            padding: "8px 55px",
            marginRight: "20px",
          }}
        >
          Next
        </button>
      </div>
    );
  }
}

export default Dashboard;
